<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;

class TagController extends Controller
{
    public function index(Request $request) {
        $search = $request->get('search');
        $tags = Tag::orderBy('updated_at', 'desc');
        if ($search != '') {
            $tags = $tags->where('name', 'like', '%'.$search.'%');
        }
        $tags = $tags->paginate(20);
        return view('admin.tag.index', compact('tags', 'search'));
    }
}
