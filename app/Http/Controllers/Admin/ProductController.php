<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $products = Product::orderBy('updated_at', 'desc');
        if ($search != '') {
            $products = $products->where('name', 'like', '%'.$search.'%');
        }
        $products = $products->paginate(20);
        return view('admin.product.index', compact('products', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.update');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        $request->validate([
            'category' => 'required',
            'product_name' => 'required',
        ]);
        
        if (is_null($product)) {
            $product = new Product();
        }

        $tags = collect();
        foreach(explode(',', $request->get('tags')) as $tag) {
            if ($tag != '')
                $tags->push(trim($tag));
        }
        
        $product->fill([
            'category' => $request->get('category'),
            'name' => $request->get('product_name'),
            'slug' => Str::slug($request->get('product_name')),
            'description' => $request->get('description'),
            'content' => $request->get('content'),
            'price' => $request->get('price') ?: 0,
            'discount' => $request->get('discount') ?: 0,
            'status' => $request->get('status'),
            'tags' => $tags->unique()->values()
        ]);

        $product->save();
        return redirect()->route('admin.product')->with('success', 'New product saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.product.detail', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.product.update', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function img_show(Product $product) {
        return view('admin.product.image-upload', compact('product'));
    }

    public function image_upload(Request $request, Product $product) {
        $datenow = Carbon::now();
        $data = collect();
        $path= '/assets/images/product';
        $slug = Str::slug($product->name);
        foreach($request->file('product_img') as $key => $file) {
            $filename = $slug;
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $newFileName = $filename.'-'.$datenow->timestamp.'-'.$key.'.'.$extension;
            $imgpath = $file->move(public_path($path), $newFileName);
            $data->push($path.'/'.$newFileName);
        }
        $product->images = $data;
        $product->save();

        return redirect(route('admin.product.show', $product))->with('success', 'Product Updated.');
    }
}
