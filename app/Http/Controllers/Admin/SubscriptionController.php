<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subscription;

class SubscriptionController extends Controller
{
    public function index(Request $request) {
        $search = $request->get('search');
        $subscriptions = Subscription::orderBy('updated_at', 'desc');
        if ($search != '') {
            $subscriptions = $subscriptions->where('name', 'like', '%'.$search.'%');
            $subscriptions = $subscriptions->orWhere('email', 'like', '%'.$search.'%');
        }
        $subscriptions = $subscriptions->paginate(20);
        return view('admin.subscription.index', compact('subscriptions', 'search'));
    }
}
