<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index(Request $request) {
        $search = $request->get('search');
        $posts = Post::orderBy('updated_at', 'desc');
        if ($search != '') {
            $posts = $posts->where('title', 'like', '%'.$search.'%');
        }
        $posts = $posts->paginate(20);
        return view('admin.post.index', compact('posts', 'search'));
    }

    public function create() {
        return view('admin.post.update');
    }

    public function show(Post $post) {
        return view('main.post.detail', compact('post'));
    }

    public function store(Request $request, Post $post) {
        $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
        
        if (is_null($post)) {
            $post = new Post();
        }

        $tags = collect();
        foreach(explode(',', $request->get('tags')) as $tag) {
            if ($tag != '')
                $tags->push(trim($tag));
        }
        $post->fill([
            'title' => $request->get('title'),
            'body' => $request->get('body'),
            'status' => $request->get('status'),
            'overview' => $request->get('overview'),
            'keywords' => $request->get('keywords'),
            'tags' => $tags->unique()->values()
        ]);
        $slug = '';
        if (is_null($post->created_at)) {
            $datenow = Carbon::now()->timestamp;
            $slug = (Str::slug($request->get('title')) ?: $request->get('title')).'-'.$datenow;
            $post->slug = $slug;
        } else {
            $slug = $post->slug;
        }

        if ($request->hasFile('banner')) {
            $post->banner = $this->upload($request->file('banner'), $slug.'-banner');
        }
        if ($request->hasFile('thumbnail')) {
            $post->thumbnail = $this->upload($request->file('thumbnail'), $slug.'-thumbnail');
        }

        $post->save();
        return redirect()->route('admin.post.show', compact('post'))->with('success', 'New post saved.');
    }

    public function edit(Post $post) {
        return view('admin.post.update', compact('post'));
    }

    public function image_upload(Request $request) {
        $datenow = Carbon::now();
        $file=$request->file('file');
        $path= '/assets/images/upload';
        $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        $newFileName = $filename.'-'.$datenow->timestamp.'.'.$extension;
        $imgpath = $file->move(public_path($path), $newFileName);
        return response()->json([
            'location' => URL($path.'/'.$newFileName)
        ]);
    }

    public function upload($file, $name = '') {
        if (empty($file))
            return '';
        
        $path= '/assets/images/upload';
        $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        $timestamp = Carbon::now()->timestamp;
        $name = $name.'-'.$timestamp.'.'.$extension;
        $imgpath = $file->move(public_path($path), $name);
        return $path.'/'.$name;
    }
    
}
