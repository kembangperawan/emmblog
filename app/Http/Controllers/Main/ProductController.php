<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index(Request $request) {
        $search = $request->get('search');
        $products = Product::orderBy('updated_at', 'desc');
        if ($search != '') {
            $products = $products->where('name', 'like', '%'.$search.'%');
        }
        $products = $products->where('status', 'active')->paginate(20);
        return view('main.product.list', compact('products', 'search'));
    }

    public function detail(Product $product) {
        return view('main.product.detail', compact('product'));
    }
}
