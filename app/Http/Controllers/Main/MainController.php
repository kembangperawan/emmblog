<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subscription;
use App\Models\Post;
use Validator;

class MainController extends Controller
{
    public function index() {

    }

    public function subscribe(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            $errors = collect();
            foreach($validator->errors()->all() as $error) {
                $errors->push($error);
            }
            return response()->json([
                'status' => 401,
                'message' => $validator->errors()->first()
            ]);
        }

        $subscription = Subscription::where('email', $request->get('email'))->first();
        if (!is_null($subscription) && $subscription->status) {
            return response()->json([
                'status' => 401,
                'message' => 'Email is exist.',
            ]);
        }

        if (is_null($subscription)) {
            $subscription = new Subscription();
            $subscription->fill([
                'name' => $request->get('name'),
                'email' => $request->get('email')
            ]);
        } else {
            $subscription->status = 1;
        }

        $subscription->save();

        return response()->json([
            'status' => 200,
            'message' => 'Added to subscriber list.',
        ]);
    }

    public function feed() {
        // create new feed
        $feed = \App::make('feed');

        // multiple feeds are supported
        // if you are using caching you should set different cache keys for your feeds

        // cache the feed for 60 minutes (second parameter is optional)
        $feed->setCache(60, 'laravelFeedKey');

        // check if there is cached feed and build new only if is not
        if (!$feed->isCached()) {
            $posts = Post::where('status', Post::PUBLISH)->orderBy('updated_at')->get();

            $feed->title = 'Live in Japan';
            $feed->description = 'All about my activities in Japan.';
            $feed->logo = 'http://yoursite.tld/logo.jpg';
            $feed->link = url('feed');
            $feed->setDateFormat('datetime');
            $feed->pubdate = $posts[0]->updated_at;
            $feed->lang = 'en';
            $feed->setShortening(true);
            $feed->setTextLimit(100);

            foreach ($posts as $post) {
                $feed->add($post->title, 'Emmanuel Sitinjak', url($post->slug), $post->publish_date, $post->overview, $post->body);
            }

        }

        // first param is the feed format
        // optional: second param is cache duration (value of 0 turns off caching)
        // optional: you can set custom cache key with 3rd param as string
        return $feed->render('atom');

        // to return your feed as a string set second param to -1
        // $xml = $feed->render('atom', -1);

    }
}
