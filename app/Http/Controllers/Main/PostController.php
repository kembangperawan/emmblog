<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Tag;

class PostController extends Controller
{
    public function index(Request $request) {
        $search = $request->get('search');
        $posts = Post::orderBy('updated_at', 'desc');
        if ($search != '') {
            $posts = $posts->where('name', 'like', '%'.$search.'%');
        }
        $posts = $posts->where('status', Post::PUBLISH)->paginate(10);
        return view('main.post.list', compact('posts', 'search'));
    }

    public function detail(Post $post) {
        return view('main.post.detail', compact('post'));
    }

    public function tag(Request $request, Tag $tag) {
        $search = $request->get('search');
        $posts = $tag->posts();
        if ($search != '') {
            $posts = $posts->where('name', 'like', '%'.$search.'%');
        }
        $posts = $posts->where('status', Post::PUBLISH)->paginate(20);
        return view('main.post.list', compact('posts', 'search'));
    }
}
