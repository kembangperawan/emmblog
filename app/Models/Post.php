<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    const DRAFT = 0;
    const PUBLISH = 1;
    const UNPUBLISHED = 2;

    protected $fillable = [
        'title',
        'body',
        'overview',
        'banner',
        'thumbnail',
        'keywords',
        'status',
        'slug',
        'tags'
    ];
    protected $casts = [
        'tags' => 'array'
    ];

    public function getJoinedTagAttribute() {
        if (count($this->tags) == 0)
            return '';
        return implode(', ', $this->tags);
    }

    public function getRouteKeyName() {
        return 'slug';
    }

    public function getPublishDateAttribute() {
        return Carbon::parse($this->created_at)->toFormattedDateString();
    }

    public function getThumbnailAttribute($value) {
        return $value ?: '';
    }

    public function getBannerAttribute($value) {
        return $value ?: '';
    }

    public function suggestions() {
        $return = Post::orderBy('updated_at')->where('id','!=', $this->id);
        $tags = $this->tags;
        $return = $return->where(function($query) use ($tags) {
            foreach($this->tags as $tag) {
                $query = $query->orWhereJsonContains('tags', $tag);
            }
        });

        return $return;
    }
}
