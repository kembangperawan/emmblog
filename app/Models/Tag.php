<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class Tag extends Model
{
    protected $fillable = [
        'name',
        'created_at',
        'updated_at'
    ];

    public function posts() {
        return Post::whereJsonContains('tags', $this->name);
    }

    public function getPostCountAttribute() {
        return $this->posts()->count();
    }

    public function getRouteKeyName() {
        return 'name';
    }
}
