<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'category',
        'name',
        'slug',
        'description',
        'content',
        'price',
        'discount',
        'status',
        'tags',
        'images',
    ];

    protected $casts = [
        'tags' => 'array',
        'images' => 'array'
    ];

    public function getJoinedTagAttribute() {
        if (count($this->tags) == 0)
            return '';
        return implode(', ', $this->tags);
    }

    public function getRouteKeyName() {
        return 'slug';
    }

    public function getImagesAttribute($value) {
        if($value) {
            return json_decode($value);
        }
        return [];
    }

    public function getThumbnailAttribute() {
        if(count($this->images) > 0)
            return collect($this->images)->first();
        return '';
    }

    public function getFinalPriceAttribute() {
        return '$ '.($this->price - $this->discount);
    }
    
    public function recommendations() {
        return Product::orderBy('updated_at')->where('status', 'active')->where('id','!=', $this->id)->where('category', $this->category);
    }
}
