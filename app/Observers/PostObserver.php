<?php

namespace App\Observers;

use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;

class PostObserver
{
    public function created(Post $post){
        $new_tags = collect();
        $tags = Tag::whereIn('name', $post->tags)->get();
        $now = Carbon::now();
        foreach($post->tags as $tag) {
            $exist = $tags->where('name', $tag)->first();
            if (is_null($exist)) {
                $new_tags->push([
                    'name' => $tag,
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            } 
        }
        Tag::insert($new_tags->toArray());
    }

    public function updated(Post $post) {
        $new_tags = collect();
        $tags = Tag::whereIn('name', $post->tags)->get();
        $now = Carbon::now();
        foreach($post->tags as $tag) {
            $exist = $tags->where('name', $tag)->first();
            if (is_null($exist)) {
                $new_tags->push([
                    'name' => $tag,
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            } 
        }
        Tag::insert($new_tags->toArray());
    }
}
