<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}" />

		<title>@yield('title', 'Emmanuel Sitinjak')</title>
		<meta property="og:title" content="@yield('title', trans('meta.title'))" />
		<meta property=”og:url” content="{{ URL::full() }}" />
		<meta property="og:type" content="blog" />
		<meta property="og:image" content="@yield('meta_image', url(trans('meta.image')))" />
		<meta name="description" content="@yield('meta_description', trans('meta.description'))">
		<meta name="keywords" content="@yield('meta_keywords', trans('meta.keywords'))">
  		<meta name="author" content="{{ trans('meta.author') }}">
		<link href="{{ URL::full() }}" rel="canonical">
		@if(Request::is('ura*'))
		<meta name="robots" content="noindex, nofollow" />
		@else
		<meta name="robots" content="index, follow" />
		@endif

		<link rel="stylesheet" href="/assets/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.0/css/all.css">
		<link href="https://fonts.googleapis.com/css?family=Dosis:300,400,600,700" rel="stylesheet">
		<link rel="stylesheet" href="/assets/css/main.css" crossorigin="anonymous">

	</head>
	<body>
		<nav id="navbar" class="navbar navbar-expand-lg sticky-top navbar-light bg-white py-3">
			<div class="container">
				<a class="navbar-brand font-weight-bolder" href="/">エム</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
					@if(Auth::check())
						<li class="nav-item">
							<a class="nav-link {{ Request::is('blog*') ? 'active' : '' }}" href="{{ route('main.post.list') }}">Blog</a>
						</li>
						<li class="nav-item">
							<a class="nav-link {{ Request::is('product*') ? 'active' : '' }}" href="{{ route('main.product.list') }}">Store</a>
						</li>
						
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								{{ Auth::user()->name }} <span class="caret"></span>
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="{{ route('admin.post') }}">Post</a>
								<a class="dropdown-item" href="{{ route('admin.product') }}">Product</a>
								<a class="dropdown-item" href="{{ route('admin.tag') }}">Tag</a>
								<a class="dropdown-item" href="{{ route('admin.subscription') }}">Subscription</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="{{ route('logout') }}"
									onclick="event.preventDefault();
													document.getElementById('logout-form').submit();">
									{{ __('Logout') }}
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</div>
						</li>
						@endif
					</ul>
				</div>
			</div>
		</nav>
		<div class="content-wrapper mb-5">
			@yield('content')
		</div>
		<!-- footer -->
		<footer>
			@if(false)
			<div class="subscribe pt-2" style="background: #EFEFEF;">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-12 py-3">
							<div class="text-center mb-3">
								<h4>Subscribe to Updates</h4>
								<div>I will send you an update by sending an email a week. Unsubscribe any time.</div>
							</div>
							<form method="POST" class="mb-5" id="subscribeform">
								<div class="form-group">
									<input type="text" name="name" class="form-control" required placeholder="Your Name" />
								</div>
								<div class="form-group">
									<input type="email" name="email" class="form-control" required placeholder="Your Email" />
								</div>
								<div>
									<button class="btn btn-primary btn-block btn-theme">Subscribe</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			@endif
			<div class="social pt-4 pb-3" style="background: #30373b;">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-12 py-3 text-center">
							<div class="text-center pb-3">
								<h5 class="mb-3 text-white">Let's connect!</h5>
								<ul class="list-inline">
									<li class="list-inline-item"><a href="mailto:surabi.eman@gmail.com" class="text-white" rel="nofollow"><i class="far fa-lg fa-envelope"></i></a></li>
									<li class="list-inline-item"><a class="text-white" href="https://www.instagram.com/emmards/" rel="nofollow" target="_blank"><i class="fab fa-lg fa-instagram"></i></a></li>
									<li class="list-inline-item"><a class="text-white" href="https://twitter.com/emmards" rel="nofollow" target="_blank"><i class="fab fa-lg fa-twitter"></i></a></li>
									<li class="list-inline-item"><a class="text-white" href="https://www.linkedin.com/in/emmanuel-ardianto-703b1a67/" rel="nofollow" target="_blank"><i class="fab fa-lg fa-linkedin"></i></a></li>
								</ul>
							</div>
							<div class="text-white">&copy; <a href="{{URL('/')}}" class="text-white">emmards</a> 2018-2019</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		@if(ENV('APP_ENV') != 'local')
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5e3cfc95aedbf846"></script>
		@endif
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="/assets/js/bootstrap.min.js" crossorigin="anonymous"></script>
		<script type="text/javascript">
			var current_scroll = 0;
			$(function() {
				$(document).on('scroll', function() {
					var st = window.pageYOffset || document.documentElement.scrollTop; 
					if (st > 0) {
						if (st > current_scroll){
							$('#navbar').removeClass('sticky-top');
						} else {
							$('#navbar').addClass('sticky-top');
						}
						$('#navbar').addClass('border-bottom');
					} else {
						$('#navbar').addClass('sticky-top');
						$('#navbar').removeClass('border-bottom');
					}
					current_scroll = st <= 0 ? 0 : st;
				});
			});

			$('#subscribeform').on('submit', function(e) {
				e.preventDefault(); 
				if ($('input[name=name]').val() == '') {
					alert('please input your name.');
					return false;
				}
				if ($('input[name=email]').val() == '') {
					alert('please input your email.');
					return false;
				}
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				$.ajax({
                    url: '{{ route('main.subscribe.register') }}',
                    type: 'POST',
                    data: {
						_token: CSRF_TOKEN, 
						name: $('input[name=name]').val(),
						email: $('input[name=email]').val()
					},
                    dataType: 'JSON',
                    success: function (data) { 
                        console.log(data);
                    }
                });
				return false;
			});
		</script>
		@yield('script')
	</body>
</html>