@extends('layouts.main')
@section('title', $post->title)
@section('meta_image', URL($post->banner?: $post->thumbnail))
@section('meta_description', $post->overview)
@section('meta_keywords', $post->keywords)

@section('content')
@if($post->banner)
<div class="paralax-banner" style="background-image: url('{{ $post->banner }}');"></div>
@endif
<div class="container my-5">
    <div class="row">
        <div class="col-12">
            <div class="blog-header">
                <h1 class="mb-3">{{ $post->title }}</h1>
                <div class="row text-muted small">
                    <div class="col-6">{{ $post->publish_date }}</div>
                    <div class="col-6 text-right">
                        @foreach($post->tags as $tag)
                        <a href="{{ route('main.post.tag', $tag) }}" class="text-secondary text-decoration-none">{{ $tag }}</a>
                        @if (!$loop->last)
                        &middot;
                        @endif
                        @endforeach
                    </div>
                    <div class="col-12 py-2"><div class="addthis_inline_share_toolbox"></div></div>
                </div>
                @if(Auth::check())
                <div class="py-3">
                    <a href="{{ route('admin.post.edit', $post) }}">Edit</a>
                </div>
                @endif
            </div>
            <div class="py-3 blog-post">
                {!! $post->body !!}
            </div>
        </div>
        <div class="col-12 py-2 text-center">
            <small>Share this:</small>
            <div class="addthis_inline_share_toolbox"></div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12">
        <h4 class="border-bottom pb-2 mb-3">More from me</h4>
        </div>
        @foreach($post->suggestions()->take(2)->get() as $key => $suggestion)
        <div class="col-md-6 col-12">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-lg-4 col-md-4 col-12" style="min-height: 150px;">
                        <a href="{{ route('main.post.detail', $suggestion) }}" class="d-block w-100 h-100 blog-thumbnail" style="background: url('{{ $suggestion->thumbnail }}');">&nbsp;</a>
                    </div>
                    <div class="col-lg-8 col-md-8 col-12">
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{ route('main.post.detail', $suggestion) }}" class="text-secondary text-decoration-none">{{ $suggestion->title }}</a></h5>
                            <p class="card-text">
                                <small class="text-muted">{{ $suggestion->publish_date }}</small>
                                <br />
                                <small class="text-muted">
                                    @foreach($suggestion->tags as $tag)
                                    <a href="{{ route('main.post.tag', $tag) }}" class="text-secondary text-decoration-none">{{ $tag }}</a>
                                    @if (!$loop->last)
                                    &middot;
                                    @endif
                                    @endforeach
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection