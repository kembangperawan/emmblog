@extends('layouts.main')
@section('content')
<div class="mb-5" style="background: url('/assets/images/main/product-photo-banner.JPG') no-repeat 100%; background-attachment: fixed; background-position: top center;">
    <div class="container" style="min-height: 300px; padding-top: 100px;">
        <div class="row">
            <div class="col-12">
                <h1 class="text-white display-2">Blog</h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        @foreach($posts as $key => $post)
        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1 col-12">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-lg-3 col-md-4 col-12" style="min-height: 150px;">
                        <a href="{{ route('main.post.detail', $post) }}" class="d-block w-100 h-100 blog-thumbnail" style="background: url('{{ $post->thumbnail }}');">&nbsp;</a>
                    </div>
                    <div class="col-lg-9 col-md-8 col-12">
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{ route('main.post.detail', $post) }}" class="text-secondary text-decoration-none">{{ $post->title }}</a></h5>
                            <p class="card-text">{{ $post->overview }}</p>
                            <p class="card-text">
                                <small class="text-muted">{{ $post->publish_date }}</small>
                                <br />
                                <small class="text-muted">
                                    @foreach($post->tags as $tag)
                                    <a href="{{ route('main.post.tag', $tag) }}" class="text-secondary text-decoration-none">{{ $tag }}</a>
                                    @if (!$loop->last)
                                    &middot;
                                    @endif
                                    @endforeach
                                </small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <div class="col-12 py-3">
        {{ $posts->appends($_GET)->links() }}
        </div>
    </div>
</div>
@endsection