<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>emmanuel_cv</title>

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    body {
        color: #3D3D3D;
        font-family: 'Roboto', sans-serif;
        font-size: 12px;
        margin: 0;
        padding: 0;
    }
    .pattern-background {
        background:
        radial-gradient(black 3px, transparent 4px),
        radial-gradient(black 3px, transparent 4px),
        linear-gradient(#fff 4px, transparent 0),
        linear-gradient(45deg, transparent 74px, transparent 75px, #a4a4a4 75px, #a4a4a4 76px, transparent 77px, transparent 109px),
        linear-gradient(-45deg, transparent 75px, transparent 76px, #a4a4a4 76px, #a4a4a4 77px, transparent 78px, transparent 109px),
        #fff;
        background-size: 109px 109px, 109px 109px,100% 6px, 109px 109px, 109px 109px;
        background-position: 54px 55px, 0px 0px, 0px 0px, 0px 0px, 0px 0px;
    }
    h1 {
        font-weight: 700;
        font-size: 1.7em;
        margin-bottom: 30px;
    }
    h2 {
        font-weight: 700;
        font-size: 1.5em;
        margin: 0;
        padding: 5px;
        color: #3D3D3D;
        border-bottom: 2px solid #3D3D3D;
    }
    table tr td {
        line-height: 1.5em;
    }
    /* table tr td:first-child {
        text-align: right;
        padding-right: 5%;
        font-weight: 700;
        width: 30%;
        vertical-align: top;
    } */
    table tr td.header {
        text-align: left;
        /* border-left: 3px solid #3D3D3D; */
        background: #3D3D3D;
        line-height: 0.5em;
    }
    ul {
        padding-left: 15px;
        margin: 0;
    }
    ul li {
        line-height: 1.7em;
    }
    .job-item {
        padding: 10px 5px;
        border-bottom: 1px dashed #DEDEDE;
    }
    .company, .grade, .skill-title {
        font-size: 1.1em;
        font-weight: bold;
    }
    .job-title, .institution {
        font-weight: bold;
        color: #3D3D3D;
    }
    .contact-info td {
        font-weight: bold;
    }
    @media print {
        .lang-item span, .light-border {
            -webkit-print-color-adjust: exact; 
        }
    }
    .lang-item span {
        width: 18px;
        display: inline-block;
        background: #3D3D3D;
        border-radius: 50%;
        margin: 0 3px;
    }
    .lang-item span.gray {
        background: #DEDEDE;
    }
    .propic {
        width: 150px;
        border-radius: 50%;
    }
    .type-of-business {
        font-weight: bold;
    }
    .light-border {
        background: #3D3D3D;
        margin-bottom: 3px;
    }
    .light-border.sm {
        height: 2px;
    }
    .light-border.lg {
        height: 20px;
    }
    .social-container img {
        width: 16px;
        vertical-align: text-bottom;
    }
    .social-name {
        font-weight: bold;
        font-size: 0.9em;
    }
    </style>
  </head>
  <body>
  <div>
      <table cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td colspan="4" style="text-align: left;" valign="top">
                <h1 style="font-size: 2em; margin: 0; padding: 20px 0; text-transform: uppercase;">Emmanuel Ardianto Sitinjak</h1>
                <!-- <p style="margin: 0 0 20px 0; font-size: 12px;">Product Manager and Front End Developer -->
                
                <!-- <br /><img src="{{URL('/assets/images/linked-in-icon.png')}}" alt="linkedin" title="linkedin" style="height: 14px; vertical-align: middle;" /> Emmanuel Ardianto</p> -->
                <table cellpadding="0" cellspacing="0" style="width: 100%;" class="contact-info">
                    <tr>
                        <td width="33.3%">i_am@emmards.me</td>
                        <td width="33.3%">http://emmards.me</td>
                        <td width="33.3%">Tokyo, Japan</td>
                    </tr>
                </table>
                <p>Experienced Product Manager and Software Engineer with a demonstrated history of working in the B2C and B2B retail industry. Skilled in analysis, UML Design, Front End Development and UI/UX. Strong in technical and product management professional with a Bachelor focused in Information Technology.</p>
            </td>
            <td align="right" valign="top">
                <img class="propic" src="/assets/images/main/propic.jpg" alt="propic" title="propic" />
            </td>
        </tr>
        <tr>
            <td width="24%"></td>
            <td width="24%"></td>
            <td width="4%"></td>
            <td width="24%"></td>
            <td width="24%"></td>
        </tr>
        <tr>
            <td colspan="2">
                <!-- work experience -->
                <h2>Experiences</h2>
                @foreach(config('about-me.work-experiences') as $key => $value)
                    @if($value['visibility'])
                    <div class="job-item">
                        <div class="company">{{$value['employer']}}</div>
                        <div class="type-of-business">{{$value['type_of_business']}}</div>
                        @foreach($value['positions'] as $key => $positions)
                        <div class="job-title">{{$positions['position']}}</div>
                        <div class="date-location">{{$positions['date']}}</div>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td width="30%" valign="top">Responsibilities</td>
                                <td valign="top">{{$positions['responsibilities']}}</td>
                            </tr>
                            @if (count($positions['project']) > 0)
                            <tr>
                                <td valign="top">Projects</td>
                                <td valign="top">
                                    <ul>
                                        {{count($positions['project']) == 0 ? '-' : ''}}
                                        @foreach($positions['project'] as $subkey => $subvalue)
                                        <li>{{$subvalue}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>
                            @endif
                        </table>
                        @endforeach
                    </div>
                    @endif
                @endforeach
                <!-- end work experience -->
            </td>
            <!-- space -->
            <td width="4%"><br /></td>
            <!-- end space -->
            <!-- 2nd column -->
            <td valign="top" width="48%"  colspan="2">
                <!-- Skills -->
                <h2>Skills</h2>
                @foreach(config('about-me.skills') as $key => $value)
                <div class="job-item">
                    <div class="skill-title">{{$value['title']}}</div>
                    <ul>
                        @foreach($value['items'] as $subkey => $subvalue)
                            <li>{{$subvalue}}</li>
                        @endforeach
                        </ul>
                </div>
                @endforeach
                <!-- end skills -->
                <!-- Language -->
                <h2>Languages</h2>
                @foreach(config('about-me.language') as $key => $value)
                <div class="job-item">
                    <div class="lang-item">
                        {{$value['name']}} 
                        <div style="float: right;">
                            @for($i=0;$i<$value['value'];$i++)
                                <span>&nbsp;</span>
                            @endfor
                            @for($i=0;$i < 4-$value['value'];$i++)
                                <span class="gray">&nbsp;</span>
                            @endfor
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- end language -->
                <!-- education -->
                <h2>Educations</h2>
                @foreach(config('about-me.education')['en'] as $key => $value)
                <div class="job-item">
                    <div class="grade">{{$value['grade']}}</div>
                    <div class="institution">{{$value['institution']}}</div>
                    <div class="date">{{$value['date']}}</div>
                </div>
                @endforeach
                <!-- end education -->
            </td>
        </tr>
      </table>
  </div>
  <div class="light-border sm"></div>
  {{-- 
  <table cellpadding="10" cellspacing="0" style="width: 100%;" class="social-container">
    <tr>
        <td width="33%">
            <img src="{{URL('/assets/images/linked-in-icon.png')}}" title="linkedin" alt="linkedin" />&nbsp;
            <span class="social-name">Emmanuel Ardianto</span>
        </td>
        <td width="33%">
            <img src="{{URL('/assets/images/fb-icon.png')}}" title="fb" alt="fb" />&nbsp;
            <span class="social-name">Emmanuel Ardianto</span>
        </td>
        <td>
            <img src="{{URL('/assets/images/twitter-icon.png')}}" title="twitter" alt="twitter" />&nbsp;
            <span class="social-name">@emmards</span>
        </td>
    </tr>
  </table>
  --}}
</body>
</html>