@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-12">
            <div class="p-2 border">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @for($i=0;$i<=7;$i++)
                        <div class="carousel-item {{ $i == 0 ? 'active' : '' }}" data-slide-number="{{ $i }}">
                            <img src="/assets/images/main/banner-{{ $i % 4 }}.jpg" class="d-block w-100" alt="banner">
                        </div>
                        @endfor
                    </div>
                    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
        
                </div>
                <div class="py-2 text-center">
                @for($i=0;$i<=7;$i++)
                    <div class="d-inline py-1 px-1">
                        <a href="#" data-target="#myCarousel" data-slide-to="{{ $i }}" class="text-decoration-none {{ $i == 0 ? 'active' : '' }}">
                            <img src="/assets/images/main/banner-{{ $i % 4 }}.jpg" alt="banner" style="width: 75px;" class="my-1 rounded">
                        </a>
                    </div>
                @endfor
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-12">
            <div class="p-2 border">
                <div class="font-weight-bold">
                    {{ $product->name }}
                </div>
                <div class="font-weight-bold">
                    {{ $product->final_price }}
                </div>
                <div class="small text-muted py-2">
                    Get discount by signing up to my subscription list. Discount coupon will be sent to your email after confirm your subscription.
                </div>
                <div class="">
                    <a href="#" class="btn btn-block btn-primary">Buy Now</a>
                </div>
            </div>
        </div>
        <div class="col-12 py-3">
            <h4 class="border-bottom pb-2 mb-3">Other Items</h4>
        </div>
        <div class="col-12">
            <div class="row">
                @foreach($product->recommendations()->take(3)->get() as $key => $recommend)
                <div class="col-lg-3 col-md-4 col-6 mb-3">
                    <div class="card">
                    <a href="{{ route('main.product.detail', $recommend) }}" class="card-text text-decoration-none text-dark"><img src="{{ $recommend->thumbnail }}" class="card-img-top" alt="card"></a>
                        <div class="card-body">
                            <h5 class="card-title text-truncate"><a href="/recommend-detail" class="card-text text-decoration-none text-dark">{{ $recommend->name }}</a></h5>
                        </div>
                        <div class="card-footer bg-transparent">
                            <div class="row">
                                <div class="col-6"><strong>{{ $recommend->final_price }}</strong></div>
                                <div class="col-6 text-right"><a href="/post" class="btn btn-primary btn-sm">Buy Now</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection