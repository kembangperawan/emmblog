@extends('layouts.main')
@section('content')
<div class="mb-5" style="background: url('/assets/images/main/product-photo-banner.JPG') no-repeat center; height: 300px; padding-top: 120px;">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="text-white display-2">Presets</h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        @foreach($products as $key => $product)
        <div class="col-lg-3 col-md-4 col-6 mb-3">
            <div class="card">
            <a href="{{ route('main.product.detail', $product) }}" class="card-text text-decoration-none text-dark"><img src="{{ $product->thumbnail }}" class="card-img-top" alt="card"></a>
                <div class="card-body">
                    <h5 class="card-title text-truncate"><a href="/product-detail" class="card-text text-decoration-none text-dark">{{ $product->name }}</a></h5>
                </div>
                <div class="card-footer bg-transparent">
                    <div class="row">
                        <div class="col-6"><strong>{{ $product->final_price }}</strong></div>
                        <div class="col-6 text-right"><a href="/post" class="btn btn-primary btn-sm">Buy Now</a></div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection