@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="text-center my-5">
                <h2>Hi! I'm em.</h2>
                <p>Product Manager and Software Engineer</p>
            </div>
            <div class="row my-5">
                <div class="col-12">
                    <div class="mb-3">
                        <img src="{{URL('/assets/images/main/about-me-banner.JPG')}}" title="this is me" alt="this is me" style="width: 100%" />
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-3 col-md-10 offset-md-1 col-12 text-center">
                    I'm a Product Manager and Software Engineer based in Tokyo, Japan. <br />I have a passion in Product Management and love to create web or mobile apps especially on front end side.
                </div>
            </div>
            <h2 class="text-center my-">What can I do</h2>
            <div class="row mb-5">
                <div class="col-lg-6 offset-lg-3 col-md-10 offset-md-1 col-12 text-center">
                    I can help you build a requirement, timeline, and system design for your Idea. Of course, I can make it happend too. Just send me your thoughts <a href="mailto:surabi.eman@gmail.com" rel="nofollow">here</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection