<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>emmanuel_cv</title>

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    body {
        color: #3D3D3D;
        font-family: 'Roboto', sans-serif;
        font-size: 12px;
        margin: 0;
        padding: 0;
    }
    .text-gray {
        color: #bdc3c7;
    }
    
    .text-right {
        text-align: right;
    }
    .float-left{
        float: left;
    }
    .left-title {
        width: 20%; 
        text-align: right; 
        padding: 0 5px;
        float: left;
    }
    .right-content {
        width: 70%; 
        border-left: 1px solid #bdc3c7; 
        padding: 0 5px;
        float: left;
    }
    .title {
        font-weight: bold;
        text-transform: uppercase;
        font-size: 1.2em;
        border-bottom: 2px solid #EFEFEF;
        padding-bottom: 5px;
        margin-bottom: 5px;
    }
    .sub-title {
        font-weight: bold;
    }
    p {
        line-height: 1.4em;
    }
    .section {
        margin-bottom: 10px;
    }
    ul {
        padding-left: 15px;
        margin: 0;
    }
    ul li {
        line-height: 1.7em;
    }
    @media print {
        .lang-item span, .light-border {
            -webkit-print-color-adjust: exact; 
        }
    }
    .lang-item span {
        width: 15px;
        display: inline-block;
        background: #3D3D3D;
        border-radius: 50%;
        margin: 0 3px;
    }
    .lang-item span.gray {
        background: #DEDEDE;
    }
    </style>
  </head>
  <body>
    <div>
        <div class="float-left" style="width: 33.3%">
            <h1 style="font-size: 1.5em; margin: 0; text-transform: uppercase;">Emmanuel Sitinjak</h1>
            <p style="margin: 0 0 20px 0; font-size: 12px;">Software Engineer</p>
        </div>
        <div class="float-left text-gray" style="width: 33.3%">
            <div class="left-title">
                Contact
            </div>
            <div class="right-content">
                <div>surabi.eman@gmail.com</div>
                <div>+81 80 3503 2875</div>
                <div>http://emmards.me/</div>
            </div>
            <div style="clear:both;margin-bottom: 30px;"></div>
        </div>
        <div class="float-left text-gray" style="width: 33.3%">
            <div class="left-title">
                Address
            </div>
            <div class="right-content">
                <div>Shinjuku Hyakunincho 1-4-24</div>
                <div>Korudo Shin Okubo 103</div>
                <div>Tokyo 169-0073</div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div style="clear:both; margin-bottom: 40px;"></div>
        <div class="float-left" style="width: 48%; margin-right: 4%;">
            <div class="section">
                <div class="title">About Me</div>
                <div class="content">
                    <p>Experienced Software Engineer and Product Manager with a demonstrated history of working in the B2C and B2B retail industry. Skilled in analysis, UML Design, Front End Development and UI/UX. Strong in technical and product management professional with a Bachelor focused in Information Technology.</p>
                </div>
            </div>
            <div class="section">
                <div class="title">Work Experiences</div>
                <div class="content">
                    @foreach(config('about-me.work-experiences') as $key => $value)
                    @if($value['visibility'])
                    <div style="padding: 5px 0;">
                        <div style="font-weight: bold;">{{$value['employer']}}</div>
                        @foreach($value['positions'] as $key => $positions)
                        <div style="padding: 3px 0;">
                            <div>{{$positions['position']}}</div>
                            <div>{{$positions['date']}}</div>
                        </div>
                        @endforeach
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            <div class="section">
                <div class="title">Freelance Works</div>
                @foreach(config('about-me.freelance') as $key => $platform)
                <div class="sub-title">{{ $key }}</div>
                <div class="content">
                    @foreach($platform as $key => $value)
                    <div style="padding: 5px 0;">
                        <div style="font-weight: bold;">{{ $value['name'] }} {{ $value['website'] != '' ? ' - '.$value['website'] : ''}}</div>
                        <div style="padding: 3px 0;">
                            <div>{{$value['feature']}}</div>
                            <div>{{$value['stack']}}</div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
        <div class="float-left" style="width: 48%;">
            <div class="section">
                <div class="title">Educations</div>
                <div class="content">
                @foreach(config('about-me.education')['en'] as $key => $value)
                    <div style="padding: 5px 0;">
                        <div style="font-weight: bold;">{{$value['grade']}}</div>
                        <div class="institution">{{$value['institution']}}</div>
                        <div class="date">{{$value['date']}}</div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="section">
                <div class="title">Skills</div>
                <div class="content">
                @foreach(config('about-me.skills') as $key => $value)
                    <div style="padding: 5px 0;">
                        <div style="font-weight: bold;">{{$value['title']}}</div>
                        <ul>
                        @foreach($value['items'] as $subkey => $subvalue)
                            <li>{{$subvalue}}</li>
                        @endforeach
                        </ul>
                    </div>
                @endforeach
                </div>
            </div>
            <div class="section">
                <div class="title">Languages</div>
                <div class="content">
                @foreach(config('about-me.language') as $key => $value)
                <div style="padding: 5px 0;">
                    <div class="lang-item">
                        {{$value['name']}} 
                        <div style="float: right;">
                            @for($i=0;$i<$value['value'];$i++)
                                <span>&nbsp;</span>
                            @endfor
                            @for($i=0;$i < 4-$value['value'];$i++)
                                <span class="gray">&nbsp;</span>
                            @endfor
                        </div>
                    </div>
                </div>
                @endforeach
                </div>
            </div>
            <div class="section">
                <div class="title">Certificates</div>
                <div class="content">
                @foreach(config('about-me.certificate') as $key => $value)
                <div style="padding: 5px 0;">
                    <div class="lang-item">
                        <div style="font-weight: bold;">{{$value['name']}}</div>
                        <div style="padding: 3px 0;">
                            <div>{{$value['date']}}</div>
                            <div>Score : {{$value['score']}}</div>
                        </div>
                    </div>
                </div>
                @endforeach
                </div>
            </div>
            <div class="section">
                <div class="title">Hobbies</div>
                <div class="content">
                    <div style="padding: 5px 0;">
                        Music, Photography, Coffee, Games, Sports
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>