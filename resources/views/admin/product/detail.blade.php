@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="mb-3">
                <span>Product Detail</span>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ route('admin.product.edit', $product) }}">Edit</a>
                </div>
            </h2>
            @include('include.alert')
            <table class="table small text-nowrap table-hover table-dropdown-hover">
                <tr>
                    <td>Name</td>
                    <td>{{ $product->name }}</td>
                </tr>
                <tr>
                    <td>Category</td>
                    <td>{{ $product->category }}</td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>{{ $product->description }}</td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td>{{ $product->price }}</td>
                </tr>
                <tr>
                    <td>Discount</td>
                    <td>{{ $product->discount }}</td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>{{ $product->status }}</td>
                </tr>
                <tr>
                    <td>Thumbnails</td>
                    <td>
                        @foreach($product->images as $image)
                        <div class="mb-1">
                            <img src="{{ $image }}" alt="{{ $product->name }}" title="{{ $product->name}}" style="width: 100px;" />
                        </div>
                        @endforeach
                        <a class="btn btn-secondary" href="{{ route('admin.product.images', $product) }}">Update</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection