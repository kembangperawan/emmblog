@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="mb-3">
                <span>Product Image</span>
            </h2>
            <form id="uploadForm" enctype="multipart/form-data" action="{{ route('admin.product.image-upload', $product) }}" method="post">
                {{ csrf_field() }}
                <div id="append">
                    <div class="py-1">
                        <input type="file" name="product_img[]" />
                    </div>
                </div>
                <div class="mb-3">
                    <button class="btn btn-secondary btn-sm" onClick="addFile()" type="button">Add</button>
                </div>
                <div class="input-group">
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    function addFile() {
        $('#append').append('<div class="py-1"><input type="file" name="product_img[]" /></div>');
    }
</script>
@endsection