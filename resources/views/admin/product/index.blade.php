@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="mb-3">
                <span>Product</span>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ route('admin.product.create') }}">New Product</a>
                </div>
            </h2>
            <div class="mb-3">
                <form method="GET" class="form-inline">
                    <div class="form-row">
                        <div class="col">
                            <input type="text" name="search" class="form-control" value="{{ $search }}" />
                        </div>
                        <div class="col">
                            <button class="btn btn-outline-dark" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            @include('include.alert')
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive-sm">
                        <table id="data" class="table small text-nowrap table-hover table-dropdown-hover">
                            <thead>
                                <tr>
                                    <th class="py-2">Name</th>
                                    <th class="py-2">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                        @foreach ($products as $item)
                                <tr>
                                    <td><a href="{{ route('admin.product.show', $item) }}">{{ $item->name }}</a></td>
                                    <td>{{ $item->status }}</td>
                                </tr>
                        @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="d-flex">
            {{ $products->appends($_GET)->links() }}
            </div>
        </div>
    </div>
</div>
@endsection