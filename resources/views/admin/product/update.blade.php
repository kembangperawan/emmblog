@extends('layouts.main')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="mb-3">
                <span>Product</span>
            </h2>
            <div>
            @include('include.alert')
            <form role="form" method="POST" action="{{ isset($product) ? route('admin.product.update', $product) : route('admin.product.store') }}" novalidate class="form-button-disabled">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="product_name" class="col-sm-3 col-form-label border-right">Name</label>
                    <div class="col-sm-9">
                        <input id="product_name" type="text" class="form-control" name="product_name" value="{{ isset($product) ? $product->name : old('name') }}" required />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="category" class="col-sm-3 col-form-label border-right">Category</label>
                    <div class="col-sm-9">
                        <input id="category" type="text" class="form-control" name="category" value="{{ isset($product) ? $product->category : old('category') }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-3 col-form-label border-right">Description</label>
                    <div class="col-sm-9">
                        <textarea id="description" style="min-height: 200px;" type="text" class="form-control" name="description">{{ isset($product) ? $product->description : old('description') }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tinycontent" class="col-sm-3 col-form-label border-right">Content</label>
                    <div class="col-sm-9">
                        <textarea id="tinycontent" style="min-height: 200px;" type="text" class="form-control" name="content">{{ isset($product) ? $product->content : old('content') }}</textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="price" class="col-sm-3 col-form-label border-right">Price</label>
                    <div class="col-sm-9">
                        <input id="price" type="number" class="form-control" name="price" value="{{ isset($product) ? $product->price : old('price') }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="discount" class="col-sm-3 col-form-label border-right">Discount</label>
                    <div class="col-sm-9">
                        <input id="discount" type="number" class="form-control" name="discount" value="{{ isset($product) ? $product->discount : old('discount') }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="title" class="col-sm-3 col-form-label border-right">Tags</label>
                    <div class="col-sm-9">
                        <input id="tags" type="text" class="form-control" name="tags" value="{{ isset($product) ? $product->joined_tag : old('tags') }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-sm-3 col-form-label border-right">Status</label>
                    <div class="col-sm-9">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status" id="inlineCheckboxInactive" value="inactive" {{ isset($product) && $product->status == 'inactive' || old('status') == 'inactive' || !isset($product) ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckboxInactive">Inactive</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status" id="inlineCheckboxActive" value="active" {{ isset($product) && $product->status == 'active' || old('status') == 'active' ? 'checked' : '' }}>
                            <label class="form-check-label" for="inlineCheckboxActive">Active</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9 offset-sm-3">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdn.tiny.cloud/1/5c7f4h2e4xyigs7jqeb41nz2ku0empfoyw9r4o9gqbjf83tf/tinymce/5/tinymce.min.js"></script>
<script type="text/javascript">
    $(function() {
        bsCustomFileInput.init()
    });

    tinymce.init({
        selector: 'textarea#tinycontent',
        plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
        ],
        toolbar: 'undo redo | styleselect | bold italic | link | alignleft aligncenter alignright | link image',
        min_height: 500,
        images_upload_handler: function (blobInfo, success, failure) {
           var xhr, formData;
           xhr = new XMLHttpRequest();
           xhr.withCredentials = false;
           xhr.open('POST', "{{ route('admin.post.image-upload') }}");
           var token = '{{ csrf_token() }}';
           xhr.setRequestHeader("X-CSRF-Token", token);
           xhr.onload = function() {
               var json;
               if (xhr.status != 200) {
                   failure('HTTP Error: ' + xhr.status);
                   return;
               }
               json = JSON.parse(xhr.responseText);

               if (!json || typeof json.location != 'string') {
                   failure('Invalid JSON: ' + xhr.responseText);
                   return;
               }
               success(json.location);
               console.log(json.location);
           };
           formData = new FormData();
           formData.append('file', blobInfo.blob(), blobInfo.filename());
           xhr.send(formData);
       }
    });
</script>
@stop