<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main.index');
})->name('home');

Route::get('/email', function () {
    return view('email.subscription');
});


Route::group([
    'namespace' => 'Main',
    'as' => 'main.',
], function ($route) {
    $route->name('subscribe.register')->post('/subscribe-register', 'MainController@subscribe');
    $route->name('about.print')->get('/about-me-print', function() {
        return view('main.about-me-print-v2');
    });

    $route->name('product.list')->get('/product', 'ProductController@index');
    $route->name('product.detail')->get('/product/{product}', 'ProductController@detail');

    $route->name('post.list')->get('/blog', 'PostController@index');
    $route->name('post.tag')->get('/blog/category/{tag}', 'PostController@tag');
    $route->name('post.detail')->get('/blog/{post}', 'PostController@detail');
    $route->name('feed')->get('/feed', 'MainController@feed');
});

Route::group([
    'namespace' => 'Admin',
    'prefix' => 'ura',
    'as' => 'admin.',
    'middleware' => ['auth']
], function ($route) {
    $route->name('post')->get('/post', 'PostController@index');
    $route->name('post.create')->get('/post/create', 'PostController@create');
    $route->name('post.store')->post('/post/create', 'PostController@store');
    $route->name('post.show')->get('/post/{post}', 'PostController@show');
    $route->name('post.edit')->get('/post/{post}/edit', 'PostController@edit');
    $route->name('post.update')->post('/post/{post}/edit', 'PostController@store');
    $route->name('post.image-upload')->post('/image-upload/', 'PostController@image_upload');

    $route->name('tag')->get('/tag', 'TagController@index');
    $route->name('subscription')->get('/subscription', 'SubscriptionController@index');

    $route->name('product')->get('/product', 'ProductController@index');
    $route->name('product.create')->get('/product/create', 'ProductController@create');
    $route->name('product.store')->post('/product/create', 'ProductController@store');
    $route->name('product.show')->get('/product/{product}', 'ProductController@show');
    $route->name('product.edit')->get('/product/{product}/edit', 'ProductController@edit');
    $route->name('product.update')->post('/product/{product}/edit', 'ProductController@store');
    $route->name('product.images')->get('/product/{product}/image-upload/', 'ProductController@img_show');
    $route->name('product.image-upload')->post('/product/{product}/image-upload/', 'ProductController@image_upload');

});
// Auth::routes();

Route::get('/ura/login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
  ]);
Route::post('/ura/login', [
'as' => '',
'uses' => 'Auth\LoginController@login'
]);
Route::post('/ura/logout', [
'as' => 'logout',
'uses' => 'Auth\LoginController@logout'
]);
